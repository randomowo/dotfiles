const char *colorname[] = {
	[0] = "#002831",
	[1] = "#e63853",
	[2] = "#5eb83c",
	[3] = "#a57706",
	[4] = "#359ddf",
	[5] = "#d75cff",
	[6] = "#4b73a2",
	[7] = "#dcdcdc",
	[8] = "#001e27",
	[9] = "#e1003f",
	[10] = "#1d9000",
	[11] = "#cd9409",
	[12] = "#006fc0",
	[13] = "#a200da",
	[14] = "#005794",
	[15] = "#e2e2e2",

	[256] = "#0e1011",
	[257] = "#7c8fa4",
	[258] = "#708284"
};

unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
    