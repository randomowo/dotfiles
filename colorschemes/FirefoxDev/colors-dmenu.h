static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#7c8fa4", "#002831" },
	[SchemeSel] = { "#7c8fa4", "#e63853" },
	[SchemeOut] = { "#7c8fa4", "#4b73a2" },
};