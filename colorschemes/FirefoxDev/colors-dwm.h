static const char *colors [][3] = {
	[SchemeNorm] = { "#7c8fa4", "#002831", "#001e27" },
	[SchemeSel] = { "#7c8fa4", "#5eb83c", "#dcdcdc" },
};