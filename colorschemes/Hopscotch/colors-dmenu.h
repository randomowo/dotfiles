static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#b9b5b8", "#322931" },
	[SchemeSel] = { "#b9b5b8", "#dd464c" },
	[SchemeOut] = { "#b9b5b8", "#149b93" },
};