static const char *colors [][3] = {
	[SchemeNorm] = { "#b9b5b8", "#322931", "#797379" },
	[SchemeSel] = { "#b9b5b8", "#8fc13e", "#b9b5b8" },
};