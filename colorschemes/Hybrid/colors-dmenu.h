static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#b7bcba", "#2a2e33" },
	[SchemeSel] = { "#b7bcba", "#b84d51" },
	[SchemeOut] = { "#b7bcba", "#7fbfb4" },
};