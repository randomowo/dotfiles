static const char *colors [][3] = {
	[SchemeNorm] = { "#b7bcba", "#2a2e33", "#1d1f22" },
	[SchemeSel] = { "#b7bcba", "#b3bf5a", "#b5b9b6" },
};