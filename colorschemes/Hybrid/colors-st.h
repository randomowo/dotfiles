const char *colorname[] = {
	[0] = "#2a2e33",
	[1] = "#b84d51",
	[2] = "#b3bf5a",
	[3] = "#e4b55e",
	[4] = "#6e90b0",
	[5] = "#a17eac",
	[6] = "#7fbfb4",
	[7] = "#b5b9b6",
	[8] = "#1d1f22",
	[9] = "#8d2e32",
	[10] = "#798431",
	[11] = "#e58a50",
	[12] = "#4b6b88",
	[13] = "#6e5079",
	[14] = "#4d7b74",
	[15] = "#5a626a",

	[256] = "#161719",
	[257] = "#b7bcba",
	[258] = "#b7bcba"
};

unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
    