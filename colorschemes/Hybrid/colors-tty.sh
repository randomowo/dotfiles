#!/bin/sh
[ "${TERM: -none}" = "linux" ] && \
printf "%b" "\e]P02a2e33
\e]P1b84d51
\e]P2b3bf5a
\e]P3e4b55e
\e]P46e90b0
\e]P5a17eac
\e]P67fbfb4
\e]P7b5b9b6
\e]P81d1f22
\e]P98d2e32
\e]PA798431
\e]PBe58a50
\e]PC4b6b88
\e]PD6e5079
\e]PE4d7b74
\e]PF5a626a
\ec"