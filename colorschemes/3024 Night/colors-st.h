const char *colorname[] = {
	[0] = "#090300",
	[1] = "#db2d20",
	[2] = "#01a252",
	[3] = "#fded02",
	[4] = "#01a0e4",
	[5] = "#a16a94",
	[6] = "#b5e4f4",
	[7] = "#a5a2a2",
	[8] = "#5c5855",
	[9] = "#e8bbd0",
	[10] = "#3a3432",
	[11] = "#4a4543",
	[12] = "#807d7c",
	[13] = "#d6d5d4",
	[14] = "#cdab53",
	[15] = "#f7f7f7",

	[256] = "#090300",
	[257] = "#a5a2a2",
	[258] = "#a5a2a2"
};

unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
    