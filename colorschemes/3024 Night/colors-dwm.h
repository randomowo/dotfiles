static const char *colors [][3] = {
	[SchemeNorm] = { "#a5a2a2", "#090300", "#5c5855" },
	[SchemeSel] = { "#a5a2a2", "#01a252", "#a5a2a2" },
};