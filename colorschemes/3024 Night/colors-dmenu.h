static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#a5a2a2", "#090300" },
	[SchemeSel] = { "#a5a2a2", "#db2d20" },
	[SchemeOut] = { "#a5a2a2", "#b5e4f4" },
};