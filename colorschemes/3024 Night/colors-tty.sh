#!/bin/sh
[ "${TERM: -none}" = "linux" ] && \
printf "%b" "\e]P0090300
\e]P1db2d20
\e]P201a252
\e]P3fded02
\e]P401a0e4
\e]P5a16a94
\e]P6b5e4f4
\e]P7a5a2a2
\e]P85c5855
\e]P9e8bbd0
\e]PA3a3432
\e]PB4a4543
\e]PC807d7c
\e]PDd6d5d4
\e]PEcdab53
\e]PFf7f7f7
\ec"