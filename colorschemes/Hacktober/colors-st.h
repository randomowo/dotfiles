const char *colorname[] = {
	[0] = "#191918",
	[1] = "#b34538",
	[2] = "#587744",
	[3] = "#d08949",
	[4] = "#206ec5",
	[5] = "#864651",
	[6] = "#ac9166",
	[7] = "#f1eee7",
	[8] = "#2c2b2a",
	[9] = "#b33323",
	[10] = "#42824a",
	[11] = "#c75a22",
	[12] = "#5389c5",
	[13] = "#e795a5",
	[14] = "#ebc587",
	[15] = "#ffffff",

	[256] = "#141414",
	[257] = "#c9c9c9",
	[258] = "#c9c9c9"
};

unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
    