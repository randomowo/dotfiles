static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#c9c9c9", "#191918" },
	[SchemeSel] = { "#c9c9c9", "#b34538" },
	[SchemeOut] = { "#c9c9c9", "#ac9166" },
};