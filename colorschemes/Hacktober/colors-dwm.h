static const char *colors [][3] = {
	[SchemeNorm] = { "#c9c9c9", "#191918", "#2c2b2a" },
	[SchemeSel] = { "#c9c9c9", "#587744", "#f1eee7" },
};