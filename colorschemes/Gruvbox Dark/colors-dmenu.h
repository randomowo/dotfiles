static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#ebdbb2", "#282828" },
	[SchemeSel] = { "#ebdbb2", "#cc241d" },
	[SchemeOut] = { "#ebdbb2", "#689d6a" },
};