static const char *colors [][3] = {
	[SchemeNorm] = { "#ebdbb2", "#282828", "#928374" },
	[SchemeSel] = { "#ebdbb2", "#98971a", "#a89984" },
};