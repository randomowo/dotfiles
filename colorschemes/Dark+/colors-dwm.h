static const char *colors [][3] = {
	[SchemeNorm] = { "#cccccc", "#000000", "#666666" },
	[SchemeSel] = { "#cccccc", "#0dbc79", "#e5e5e5" },
};