const char *colorname[] = {
	[0] = "#000000",
	[1] = "#cd3131",
	[2] = "#0dbc79",
	[3] = "#e5e510",
	[4] = "#2472c8",
	[5] = "#bc3fbc",
	[6] = "#11a8cd",
	[7] = "#e5e5e5",
	[8] = "#666666",
	[9] = "#f14c4c",
	[10] = "#23d18b",
	[11] = "#f5f543",
	[12] = "#3b8eea",
	[13] = "#d670d6",
	[14] = "#29b8db",
	[15] = "#e5e5e5",

	[256] = "#0e0e0e",
	[257] = "#cccccc",
	[258] = "#ffffff"
};

unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
    