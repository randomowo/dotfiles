static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#cccccc", "#000000" },
	[SchemeSel] = { "#cccccc", "#cd3131" },
	[SchemeOut] = { "#cccccc", "#11a8cd" },
};