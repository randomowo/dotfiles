static const char *colors [][3] = {
	[SchemeNorm] = { "#a0a0a0", "#383838", "#474747" },
	[SchemeSel] = { "#a0a0a0", "#666666", "#d0b8a3" },
};