#!/bin/sh
[ "${TERM: -none}" = "linux" ] && \
printf "%b" "\e]P0383838
\e]P1a95551
\e]P2666666
\e]P3a98051
\e]P4657d3e
\e]P5767676
\e]P6c9c9c9
\e]P7d0b8a3
\e]P8474747
\e]P9a97775
\e]PA8c8c8c
\e]PBa99175
\e]PC98bd5e
\e]PDa3a3a3
\e]PEdcdcdc
\e]PFd8c8bb
\ec"