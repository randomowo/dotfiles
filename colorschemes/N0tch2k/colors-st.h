const char *colorname[] = {
	[0] = "#383838",
	[1] = "#a95551",
	[2] = "#666666",
	[3] = "#a98051",
	[4] = "#657d3e",
	[5] = "#767676",
	[6] = "#c9c9c9",
	[7] = "#d0b8a3",
	[8] = "#474747",
	[9] = "#a97775",
	[10] = "#8c8c8c",
	[11] = "#a99175",
	[12] = "#98bd5e",
	[13] = "#a3a3a3",
	[14] = "#dcdcdc",
	[15] = "#d8c8bb",

	[256] = "#222222",
	[257] = "#a0a0a0",
	[258] = "#aa9175"
};

unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
    