static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#a0a0a0", "#383838" },
	[SchemeSel] = { "#a0a0a0", "#a95551" },
	[SchemeOut] = { "#a0a0a0", "#c9c9c9" },
};