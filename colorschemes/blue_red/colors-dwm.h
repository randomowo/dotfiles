static const char *colors [][3] = {
	[SchemeNorm] = { "#aec7df", "#0b0b0c", "#798b9c" },
	[SchemeSel] = { "#aec7df", "#8D5972", "#aec7df" },
};