static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#aec7df", "#0b0b0c" },
	[SchemeSel] = { "#aec7df", "#575367" },
	[SchemeOut] = { "#aec7df", "#6B8DA7" },
};