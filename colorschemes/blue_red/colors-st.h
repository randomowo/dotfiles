const char *colorname[] = {
	[0] = "#0b0b0c",
	[1] = "#575367",
	[2] = "#8D5972",
	[3] = "#38628D",
	[4] = "#5B6F8F",
	[5] = "#A16B97",
	[6] = "#6B8DA7",
	[7] = "#aec7df",
	[8] = "#798b9c",
	[9] = "#575367",
	[10] = "#8D5972",
	[11] = "#38628D",
	[12] = "#5B6F8F",
	[13] = "#A16B97",
	[14] = "#6B8DA7",
	[15] = "#aec7df",

	[256] = "#0b0b0c",
	[257] = "#aec7df",
	[258] = "#aec7df"
};

unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
    