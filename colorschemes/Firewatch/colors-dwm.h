static const char *colors [][3] = {
	[SchemeNorm] = { "#9ba2b2", "#585f6d", "#585f6d" },
	[SchemeSel] = { "#9ba2b2", "#5ab977", "#e6e5ff" },
};