static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#9ba2b2", "#585f6d" },
	[SchemeSel] = { "#9ba2b2", "#d95360" },
	[SchemeOut] = { "#9ba2b2", "#44a8b6" },
};