const char *colorname[] = {
	[0] = "#585f6d",
	[1] = "#d95360",
	[2] = "#5ab977",
	[3] = "#dfb563",
	[4] = "#4d89c4",
	[5] = "#d55119",
	[6] = "#44a8b6",
	[7] = "#e6e5ff",
	[8] = "#585f6d",
	[9] = "#d95360",
	[10] = "#5ab977",
	[11] = "#dfb563",
	[12] = "#4c89c5",
	[13] = "#d55119",
	[14] = "#44a8b6",
	[15] = "#e6e5ff",

	[256] = "#1e2027",
	[257] = "#9ba2b2",
	[258] = "#f6f7ec"
};

unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
    