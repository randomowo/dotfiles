static const char *colors [][3] = {
	[SchemeNorm] = { "#c2b790", "#202020", "#676767" },
	[SchemeSel] = { "#c2b790", "#728271", "#b9aa99" },
};