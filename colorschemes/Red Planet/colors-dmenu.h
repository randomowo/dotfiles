static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#c2b790", "#202020" },
	[SchemeSel] = { "#c2b790", "#8c3432" },
	[SchemeOut] = { "#c2b790", "#5b8390" },
};