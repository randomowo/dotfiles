let background = "#131314"
let foreground = "#d4c3a3"
let cursor = "#d4c3a3"

let color0 = "#131314"
let color1 = "#2D5753"
let color2 = "#62635E"
let color3 = "#A2342E"
let color4 = "#A55A2F"
let color5 = "#B3423B"
let color6 = "#A25648"
let color7 = "#d4c3a3"
let color8 = "#948872"
let color9 = "#2D5753"
let color10 = "#62635E"
let color11 = "#A2342E"
let color12 = "#A55A2F"
let color13 = "#B3423B"
let color14 = "#A25648"
let color15 = "#d4c3a3"
