static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#d4c3a3", "#131314" },
	[SchemeSel] = { "#d4c3a3", "#2D5753" },
	[SchemeOut] = { "#d4c3a3", "#A25648" },
};