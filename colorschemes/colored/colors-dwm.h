static const char *colors [][3] = {
	[SchemeNorm] = { "#d4c3a3", "#131314", "#948872" },
	[SchemeSel] = { "#d4c3a3", "#62635E", "#d4c3a3" },
};