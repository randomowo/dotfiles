const char *colorname[] = {
	[0] = "#131314",
	[1] = "#2D5753",
	[2] = "#62635E",
	[3] = "#A2342E",
	[4] = "#A55A2F",
	[5] = "#B3423B",
	[6] = "#A25648",
	[7] = "#d4c3a3",
	[8] = "#948872",
	[9] = "#2D5753",
	[10] = "#62635E",
	[11] = "#A2342E",
	[12] = "#A55A2F",
	[13] = "#B3423B",
	[14] = "#A25648",
	[15] = "#d4c3a3",

	[256] = "#131314",
	[257] = "#d4c3a3",
	[258] = "#d4c3a3"
};

unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
    