static const char *colors [][3] = {
	[SchemeNorm] = { "#bbc2cf", "#444444", "#666666" },
	[SchemeSel] = { "#bbc2cf", "#98be65", "#bbc2cf" },
};
