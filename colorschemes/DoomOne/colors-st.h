const char *colorname[] = {
	[0] = "#444444",
	[1] = "#ff6c6b",
	[2] = "#98be65",
	[3] = "#ecbe7b",
	[4] = "#a9a1e1",
	[5] = "#c678dd",
	[6] = "#51afef",
	[7] = "#bbc2cf",
	[8] = "#666666",
	[9] = "#ff6655",
	[10] = "#99bb66",
	[11] = "#ecbe7b",
	[12] = "#a9a1e1",
	[13] = "#c678dd",
	[14] = "#51afef",
	[15] = "#bfbfbf",

	[256] = "#282c34",
	[257] = "#bbc2cf",
	[258] = "#51afef"
};

unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;

