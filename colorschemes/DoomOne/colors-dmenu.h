static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = { "#bbc2cf", "#444444" },
	[SchemeSel] = { "#bbc2cf", "#ff6c6b" },
	[SchemeOut] = { "#bbc2cf", "#51afef" },
};
