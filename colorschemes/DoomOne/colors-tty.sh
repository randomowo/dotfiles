#!/bin/sh
[ "${TERM: -none}" = "linux" ] && \
printf "%b" "\e]P0444444
\e]P1ff6c6b
\e]P298be65
\e]P3ecbe7b
\e]P4a9a1e1
\e]P5c678dd
\e]P651afef
\e]P7bbc2cf
\e]P8666666
\e]P9ff6655
\e]PA99bb66
\e]PBecbe7b
\e]PCa9a1e1
\e]PDc678dd
\e]PE51afef
\e]PFbfbfbf
\ec"
