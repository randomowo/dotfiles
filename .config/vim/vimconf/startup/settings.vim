"     _____      _   _   _
"    / ____|    | | | | (_)
"   | (___   ___| |_| |_ _ _ __   __ _ ___
"   \___ \ / _ \ __| __| | '_ \ / _` / __|
"   ____) |  __/ |_| |_| | | | | (_| \__ \
"  |_____/ \___|\__|\__|_|_| |_|\__, |___/
"                                __/ |
"                               |___/
" by randomowo

set number relativenumber
set wm=0
set textwidth=120
set colorcolumn=120
set encoding=utf-8
set cmdheight=1
set shortmess+=c
set path+=*/*
set hlsearch
set smartcase
set incsearch
set cindent
set smarttab
set shiftwidth=4
set tabstop=4
set expandtab
set secure
set ruler
set list
set splitbelow
set splitright
set noswapfile
set nopaste

syntax on
colorscheme gruvbox

" make background transparent
hi! Normal ctermbg=NONE guibg=NONE

" Highlight extra whitespaces at the end
highlight ExtraWhitespace ctermbg=red guibg=red
au BufNew,BufEnter,BufWinEnter,WinEnter,BufNew * match ExtraWhitespace /\s\+$/

" ====Plugins====

" Coc.vim
" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')



" YcM
" let g:ycm_key_list_select_completion = ['<F2>', '<Down>']
" let g:ycm_key_list_previous_completion = ['<Up>']

" LaTex
let g:livepreview_previewer = 'zathura'
let g:tex_flavor = 'latex'
let g:vimtex_view_method = 'zathura'
let g:vimtex_quickfix_mode = 0

" ale
let g:ale_fix_on_save = 0
let g:ale_linters = {'python' : ['flake8', 'pylint']}
let b:ale_fixers = {
           \ 'python': ['isort', 'autopep8', 'reorder-python-imports'],
           \ 'c': ['uncrustify'],
           \ 'json': ['fixjson'],
           \ 'latex': ['textlint'],
           \ 'xml': ['xmllint']}

" lightline
let g:lightline = { 'colorscheme': 'gruvbox' }

" minisnip
let g:minisnip_trigger = '<S-Tab>'
let g:minisnip_dir = '~/.config/vim/vimconf/minisnip'

" markdown-preview
let g:mkdp_auto_start = 0
let g:mkdp_auto_close = 1
let g:mkdp_refresh_slow = 1

" ranger
let g:ranger_map_keys = 0

" vimwiki
let g:vimwiki_list = [{'path': '~/documents/vimwiki/',}]

" vim-indent-guides
let g:indent_guides_enable_on_vim_startup = 1
hi IndentGuidesOdd  ctermbg=black
hi IndentGuidesEven ctermbg=darkgrey

" FZF
" let g:fzf_layout = { 'window': 'call CreateCenteredFloatingWindow()' }
" let $FZF_DEFAULT_OPTS="--reverse "

