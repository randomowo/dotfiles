#!/bin/bash

# install yay
cd ~/downloads
git clone https://aur.archlinux.org/yay.git
[ -d ~/downloads/yay ] && cd ~/downloads/yay || exit 1
makepkg -sri

# install pip
cd ~/downloads
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
yay -Syu python
[ -f ~/get-pip.py ] && sudo python get-pip.py --no-setuptools || exit 1

# install packages
yay -Syu $(cat ~/dotfiles/yaypackage-list)
