import json
import os
import sys
import shutil


def tty(colors, path):
    file = path + "/colors-tty.sh"
    ocolors = ""
    lst = ["0", "1", "2", "3", "4", "5", "6", "7",
           "8", "9", "A", "B", "C", "D", "E", "F"]
    for i in range(len(lst)):
        ocolors += "\\e]P{0}{1}\n".format(lst[i], colors["colors"][f"color{i}"][1::])
    ocolors += "\\ec\""
    output = "#!/bin/sh\n[ \"{0}\" = \"linux\" ] && \\\nprintf \"%b\" \"{1}".format("${TERM: -none}", ocolors)
    with open(file, "w") as f:
        f.write(output)


def dmenu(colors, path):
    file = path + "/colors-dmenu.h"
    norm = "\t[SchemeNorm] = { \""
    norm += colors["special"]["foreground"]
    norm += "\", \"" + colors["colors"]["color0"] + "\" },\n"
    sel = "\t[SchemeSel] = { \""
    sel += colors["special"]["foreground"]
    sel += "\", \"" + colors["colors"]["color1"] + "\" },\n"
    out = "\t[SchemeOut] = { \""
    out += colors["special"]["foreground"]
    out += "\", \"" + colors["colors"]["color6"] + "\" },\n"""
    last = "{\n" + norm + sel + out + "};"
    output = f"static const char *colors[SchemeLast][2] = {last}"
    with open(file, "w") as f:
        f.write(output)


def dwm(colors, path):
    file = path + "/colors-dwm.h"
    norm = "\t[SchemeNorm] = { "
    norm += "\"" + colors["special"]["foreground"] + "\", "
    norm += "\"" + colors["colors"]["color0"] + "\", "
    norm += "\"" + colors["colors"]["color8"] + "\" "
    norm += "},\n"
    sel = "\t[SchemeSel] = { "
    sel += "\"" + colors["special"]["foreground"] + "\", "
    sel += "\"" + colors["colors"]["color2"] + "\", "
    sel += "\"" + colors["colors"]["color7"] + "\" "
    sel += "},\n"
    clrs = "{\n" + norm + sel + "};"
    output = f"static const char *colors [][3] = {clrs}"
    with open(file, "w") as f:
        f.write(output)


def st(colors, path):
    file = path + "/colors-st.h"
    colorname = "{\n"
    for i in range(16):
        colorname += "\t[{0}] = \"{1}\",\n".format(i, colors["colors"][f"color{i}"])
    colorname += "\n"
    colorname += "\t[256] = \"{}\",\n".format(colors["special"]["background"])
    colorname += "\t[257] = \"{}\",\n".format(colors["special"]["foreground"])
    colorname += "\t[258] = \"{}\"\n".format(colors["special"]["cursor"])
    colorname += "};"
    output = f"""const char *colorname[] = {colorname}

unsigned int defaultbg = 256;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
    """
    with open(file, "w") as f:
        f.write(output)


def vim(colors, path):
    file = path + "/colors-vim.vim"
    output = "let background = \"{}\"\n".format(colors["special"]["background"])
    output += "let foreground = \"{}\"\n".format(colors["special"]["foreground"])
    output += "let cursor = \"{}\"\n\n".format(colors["special"]["cursor"])
    for i in range(16):
        output += "let color{0} = \"{1}\"\n".format(i, colors["colors"][f"color{i}"])
    with open(file, "w") as f:
        f.write(output)


arg = sys.argv[1]
path = arg.split(".")[0]

with open(arg, "r") as json_file:
    data = json.load(json_file)
    try:
        if (not os.path.isdir(path)):
            os.mkdir(path)
    except OSError:
        print("Creation of the directory {} failed".format(path))
    else:
        tty(data, path)
        dmenu(data, path)
        dwm(data, path)
        st(data, path)
        vim(data, path)
        shutil.copyfile(arg, path + "/colors.json")
