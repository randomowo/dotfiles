import json
import sys

colors = {
    "special": {
        "background": "",
        "foreground": "",
        "cursor": ""
    },
    "colors": {
        "color0": "",
        "color1": "",
        "color2": "",
        "color3": "",
        "color4": "",
        "color5": "",
        "color6": "",
        "color7": "",
        "color8": "",
        "color9": "",
        "color10": "",
        "color11": "",
        "color12": "",
        "color13": "",
        "color14": "",
        "color15": "",
    }
}

arg = sys.argv[1]
with open(arg, 'r') as file:
    for line in file.readlines():
        name, color = line.split(' ')
        if "color" in name:
            colors['colors'][name] = color[:-1]
        elif name in colors['special']:
            colors['special'][name] = color[:-1]

with open(arg.split(".")[0]+".json", "w") as json_file:
    json.dump(colors, json_file)
